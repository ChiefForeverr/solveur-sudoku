Ce projet est le solveur de Sudoku de Hbabou Sami,en L1-C Informatique,avec le numéro d'étudiant 22001030

Ce dossier contient deux fichiers python,un étant une grille de sudoku qu'un joueur doit compléter,et l'autre étant la même grille qui sera immédiatement résolu par une IA. Le premier fichier donne une grille par défaut,et permet de choisir une "cellule" de la grille de sudoku qui sera surlignée en rouge,et de placer un nombre en utilisant le clavier.Il est possible d'effacer le nombre de n'importe quelle cellule en la choissiant et en appyuant sur la touche retour. En cas d'erreur sur la grille,un ou plusieurs messages sur le terminal l'indiqueront. Le but est donc de remplir la grille en respectant les règles,sans se prendre de message d'erreur.Le fichier avec le solveur est plus direct,dès que l'utilisateur choisit une cellule,l'IA complète immédiatement (si cela est possible) le reste de la grille en utilisant un algorithme de retour en arrière. J'aurais pu rajouter plus d'indications visuelles,tel qu'un menu principal,des messages lors des erreurs ou un message lorsque la grille est terminé,mais comme c'est secondaire et que je suis actuellement surchargé,je ne l'ai pas fait.

Les deux sont plus ou moins terminés,il me reste tout de même certaines erreurs dans le code que j'aimerais corriger et pourquoi pas essayer de rajouter plus de contenu si je le peux

Je remercie et souhaiterais citer:

Brian Knapp: https://www.youtube.com/watch?v=Xw0xxvyxFuE
TechwithTim: https://www.youtube.com/watch?v=lK4N8E6uNr4
OpenGenus : https://iq.opengenus.org/backtracking-sudoku/

Leur contenu m'ont aidé à implémenter l'algorithme utilisé dans mon programme,et m'a aidé à créer la grille sur pygame.



